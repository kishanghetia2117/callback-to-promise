const fs = require('fs');
const path = require('path');

function readFilePromises(dirPath, fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(dirPath, fileName), 'utf-8', (err, data) => {
            if (err) {
                reject(Error(`Error in reading the file \n${err}`))
            } else {
                resolve(data)
            }
        })
    })
}

function writeFilePromise(dirPath, fileName, writeData) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(dirPath, fileName), writeData, (err) => {
            if (err) {
                reject(Error(`Error in writing file \n${err}`))
            } else {
                resolve()
            }
        })
    })
}

function appendFilePromise(dirPath, fileName, appendData) {
    return new Promise((resolve, reject) => {
        fs.appendFile(path.join(dirPath, fileName), appendData, (err) => {
            if (err) {
                reject(Error(`error in appending file \n${err}`))
            } else {
                resolve()
            }
        })
    })
}

function deleteFilePromise(dirPath, files) {
    return new Promise((resolve, reject) => {
        files.forEach(file => {
            fileName = "./" + file;
            fs.unlink(path.join(dirPath, fileName), (err) => {
                if (err) {
                    reject(Error(`error in deleting the file\n${err}`))
                } else {
                    resolve()
                }
            })
        })
    })
}

function playWithFile(dirPath, fileList) {
    readFilePromises(dirPath, '/lipsum.txt')
        .then((fileData) => {
            console.log(`1. '/lipsum.txt' file has been read`)
            let dataInUpperCase = fileData.toUpperCase()
            return writeFilePromise(dirPath, 'uppercase.txt', dataInUpperCase)
        })
        .then(() => {
            console.log(`2. 'uppercase.txt': converted '/lipsum.txt' to uppercase`)
            return writeFilePromise(dirPath, fileList, 'uppercase.txt')
        })
        .then(() => {
            console.log('3. stored uppercase.txt filename in fileList.txt')
            return readFilePromises(dirPath, 'uppercase.txt')
        })
        .then((fileData) => {
            console.log('4. uppercase file has been read')
            let toLowerCase = fileData.toLowerCase().replaceAll("\n",'').split(". ").join('\n')
            return writeFilePromise(dirPath, 'lowercase.txt', toLowerCase)
        })
        .then(() => {
            console.log('5. converted to Lowercase and saved as lowercase.txt')
            return appendFilePromise(dirPath, fileList, '\nlowercase.txt')
        })
        .then(() => {
            console.log('6. appended Lowercase.txt filename to fileList.txt')
            return readFilePromises(dirPath, 'lowercase.txt')
        })
        .then((fileData) => {
            console.log('7. lowercase file has been read')
            sortedData = fileData.split('\n').sort().join('\n');
            return writeFilePromise(dirPath, 'sorted.txt', sortedData)
        })
        .then(() => {
            console.log('8. sorted lowercase.txt file and saved as sorted.txt')
            return appendFilePromise(dirPath, fileList, '\nsorted.txt')
        })
        .then(() => {
            console.log('9. appended sorted.txt filename to fileList.txt')
            return readFilePromises(dirPath, fileList)
        })
        .then((fileData) => {
            console.log("10. begin to read fileList.txt")
            let files = fileData.split('\n')
            return deleteFilePromise(dirPath, files)
        })
        .then(() => {
            console.log("11. files from fileList.txt are deleted")
        })
        .catch((err) => {
            console.log(err)
        })
}


module.exports = playWithFile;