let fs = require('fs');
const { resolve } = require('path/posix');

function dirCreatePromise(dirName) {
    return new Promise((resolve, reject) => {
        fs.mkdir(dirName, (err) => {
            if (err) {
                reject(Error(`Error in dir creation \n${err}`))
            } else {
                resolve(`${dirName} has been created sucessfully`)
            }
        })
    })
}

function fileCreatePromise(filePath) {
    return new Promise((resolve, reject) => {
        filePath.forEach((file) => {
            fs.writeFile(file, "Destroyed after creation", (err) => {
                if (err) {
                    reject(Error(`Error in file creation \n${err}`))
                } else {
                    resolve(`${filePath} has been created`)
                }
            })
        })
    })
}

function fileDestoryePrmoise(filePath) {
    return new Promise((resolve, reject) => {
        filePath.forEach((file) => {
            fs.unlink(file, (err) => {
                if (err) {
                    reject(Error(`Error in file destruction \n${err}`))
                } else {
                    resolve(`${filePath} has been Destroyed`)
                }
            })
        })
    })
}
function jsonCreateDestroy(dirName, filePath) {
    dirCreatePromise(dirName)
        .then((result) => {
            console.log(result)
            return fileCreatePromise(filePath);
        })
        .then((result) => {
            console.log(result)
            return fileDestoryePrmoise(filePath);
        })
        .then((result) => {
            console.log(result)
            return fileDestoryePrmoise(filePath);
        })
        .catch((err) => {
            console.log(err);
        })
}
module.exports = jsonCreateDestroy;



